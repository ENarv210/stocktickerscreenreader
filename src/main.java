import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;
import javax.imageio.ImageIO;
public class main {
	
	public static int startXWidth = 10;
	public static int startYHeight = 10;
	public static int endXWidth = 10;
	public static int endYHeight = 10;
	public static String imageFileLocation; //"/Users/ericnarv210/Documents/BitBucket/StockTickerScreenReader/stockTickerRip.jpg";
	public static String lastTick = "000000.00000000";
	public static String currentTick = "000001.0000000";
	public static boolean showTime = false;
	public static String decimalFormat = "000.000";
	public static String databaseName = "";
	public static String databaseIP = "";
	
	public static DecimalFormat formatter = new DecimalFormat(".000000");
	
	public static void main(String[] args) {
		
		//System.out.println(args.length);
		
		//Trace Mouse Coordinates//
		if(args.length == 1)
		{
			if(args[0].equals("0"))
			{
				PointerInfo a = MouseInfo.getPointerInfo();
				Point b  = a.getLocation();
				int x = (int)b.getX();
				int y = (int)b.getY();
				
				System.out.println("TRACING MOUSE X: " + b.x + "  Y:" + b.y);
			}
		}
		if(args.length == 10)
		{
		//	System.out.println("ENTERED");
			
			if(args[0].equals("Start"))
			{
					//Set Screen Crop Location//		
					startXWidth = Integer.parseInt(args[1]);
					startYHeight = Integer.parseInt(args[2]);
					endXWidth = Integer.parseInt(args[3]);
					endYHeight = Integer.parseInt(args[4]);
					imageFileLocation = String.valueOf(args[5]);
					showTime = Boolean.valueOf(args[6]);
					decimalFormat = String.valueOf(args[7]);
					databaseIP = String.valueOf(args[8]);
					databaseName = String.valueOf(args[9]);
					
					if(decimalFormat != null && decimalFormat != "")
					{
						formatter = new DecimalFormat(decimalFormat);
					}
					
					//Create Timer//
					Timer timerLoop = new Timer();
					timerLoop.scheduleAtFixedRate(new TimerTask() {
						  @Override
						  public void run() {
					//	    System.out.println("TIMER");
						    
						    //Take Screen Shot//
							takeScreenShot();
							
							//Process Text//
							Image image;
							try {
								image = ImageIO.read(new File(imageFileLocation));
						
							
						/*		String dataRead = mainOCRScanner.scan(
										image, 
										0, 0, image.getWidth(null), image.getHeight(null), 
										null);
								
								System.out.println("OUTPUT: " + dataRead);*/
								
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
							
						  }
						}, 0, 70);
			}
		}
		//System.out.println("DONE");
	}
	
	public static void takeScreenShot()
	{
		BufferedImage image = null;
		BufferedImage croppedImage = null;
		
			try {
				image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			} catch (HeadlessException e1) {
				e1.printStackTrace();
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
		
			try {
				//File to save Screen Shot//
				File screenShotFile = new File(imageFileLocation);

				//xPos,yPos,width,height//
				Rectangle rectangle = new Rectangle(startXWidth,startYHeight,endXWidth,endYHeight);
				croppedImage = image.getSubimage(startXWidth, startYHeight, rectangle.width, rectangle.height);
		
				//Save Screenshot Cropped Image//
				//Create File if it doesn't exist//
//				if (screenShotFile.createNewFile()) {
//					System.out.println("File is created!");
//				} else {
//					System.out.println("File already exists!");
//				}
				ImageIO.write(croppedImage, "jpg", screenShotFile);
				
			//Saved Location//
//			System.out.println("Saved at: " + screenShotFile.getPath());
			
			//Read Image Saved//
			readImageText();
			
			} catch (IOException e) {
				e.printStackTrace();
			}	
	}
	
	public static void readImageText()
	{
        Process proc = null;
		try {
			proc = Runtime.getRuntime().exec("tesseract " + imageFileLocation + " stdout");
		} catch (IOException e) {
			e.printStackTrace();
		}

        // Read the output
        BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

        String line = "";
        try {
			while((line = reader.readLine()) != null && (!line.equals(""))) {
				
				//Reading Error Corrections//
				line = line.replaceAll(" ", "");
				line = line.replaceAll("-", "");
				line = line.replaceAll("`", "");
				line = line.replaceAll("'", "");
				line = line.replaceAll(";", "");
				line = line.replaceAll(",", "");
				line = line.replaceAll("‘", "");
				line = line.replaceAll("\\+", "");
				line = line.replaceAll("=", "");
				line = line.replaceAll("E", "");
			
				//Different Number//
				currentTick = formatter.format(Double.parseDouble(line));
				
				if(currentTick != lastTick)
				{
					//Show Timestamp//
					if(showTime && (!lastTick.equals(currentTick)))
					{
						//Timestamp//
						java.util.Date date= new java.util.Date();
						DateFormat df2 = new SimpleDateFormat("HH:mm:ss.S");
						String formattedDate = df2.format(date);
						
						lastTick = formatter.format(Double.parseDouble(lastTick));
						currentTick = formatter.format(Double.parseDouble(currentTick));
						
						String lastTick_  = formatter.format(Double.parseDouble(lastTick));
						String currentTick_  = formatter.format(Double.parseDouble(currentTick));
						
						//Visual Change//
						String visualChange = "";
						String change = "";
						
						if(Double.valueOf(currentTick) >= Double.valueOf(lastTick))
						{
							change = formatter.format(Double.valueOf(currentTick_) - Double.valueOf(lastTick_));	
							visualChange = visualChange + "+" + change;
						}
						else if(Double.valueOf(currentTick) < Double.valueOf(lastTick))
						{
							
							change = formatter.format(Double.valueOf(lastTick_) - Double.valueOf(currentTick_));
							visualChange = visualChange + "-" + change;
						}
						
						//Update Comp Reference//
						lastTick = formatter.format(Double.parseDouble(currentTick));
						
						//Set Trigger Buy/Sell With Sound//
						//if(Math.abs(Double.parseDouble(change)) > .05)
					//	{
							//Output//
							if(Double.parseDouble(change) >0)
							{
								//Update Database//
								updateDBEntry(currentTick_);
								
								System.out.println(formattedDate + "  " + currentTick_ + " : " + visualChange);
							}
							else
							{
								//Update Database//
								updateDBEntry(currentTick_);
								
								System.out.println(formattedDate + "  " + currentTick_ + " : " + visualChange);
							}
					//	}
						
					}
					//Hide Timestamp
					else if(!showTime && lastTick != currentTick)
					{
						//Update Comp Reference//
						lastTick = formatter.format(currentTick);
						
						//Output//
					    System.out.print(line + "\n");
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

        try {
			proc.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}   
	}
	
	public static void updateDBEntry(String ExecutionPrice)
	{
		String user = "update_account";
		String password = "Trading2016";
		String url = "jdbc:mysql://" + databaseIP + ":3306/" + databaseName;
		Statement statement = null;
		
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			statement = con.createStatement();
			statement.executeUpdate("INSERT into `trade_logs`(`trade_price`) values ('" + ExecutionPrice + "')");		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
